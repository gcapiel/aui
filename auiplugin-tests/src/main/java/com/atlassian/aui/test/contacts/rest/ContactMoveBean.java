package com.atlassian.aui.test.contacts.rest;

import java.net.URI;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings ({ "UnusedDeclaration" })
@XmlRootElement (name = "version")
public class ContactMoveBean
{

    @XmlElement
    public URI after;

    @XmlElement
    public Position position;


    //Needed so that JAXB works.
    public ContactMoveBean()
    {}

    /**
     * Absolute positions that a version may be moved to
     */
    public enum Position
    {
        Earlier,
        Later,
        First,
        Last;
    }
}
