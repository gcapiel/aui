package it.com.atlassian.aui.javascript.pages;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.aui.component.dropdown2.AuiDropdown2;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import org.openqa.selenium.By;

import javax.inject.Inject;

public class Dropdown2TestPage implements Page {

    @Inject
    PageBinder binder;
    
    @Inject
    PageElementFinder elementFinder;

    @Override
    public String getUrl() {
        return "/plugins/servlet/ajstest/test-pages/dropdown2/dropdown2-test.html";
    }
    
    public PageElementFinder getElementFinder()
    {
        return elementFinder;
    }
    
    public AuiDropdown2 getStandardDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown2-test-match-width-trigger"));
    }
    
    public AuiDropdown2 getCustomDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown2-test-custom-width-trigger"));
    }

    public AuiDropdown2 getDisabledToolbarDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown2-toolbar-disabled-trigger"));
    }
    
    public PageElement getDisableDropdownButton()
    {
        return elementFinder.find(By.id("toggleDropdownDisabled"));
    }

    public PageElement getCustomDropdownContainerElement()
    {
        return elementFinder.find(By.id("container"));
    }

    public AuiDropdown2 getCustomContainedDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown-custom-contained-trigger"));
    }

    public AuiDropdown2 getUnstyledDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("unstyled-dropdown-trigger"));
    }

    public AuiDropdown2 getFauxFormDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown2-faux-forms-trigger"));
    }

    public AuiDropdown2 getDropdownGroupTrigger1()
    {
        return binder.bind(AuiDropdown2.class, By.id("aui-dropdown2-group-trigger1"));
    }

    public AuiDropdown2 getDropdownGroupTrigger2()
    {
        return binder.bind(AuiDropdown2.class, By.id("aui-dropdown2-group-trigger2"));
    }

    public AuiDropdown2 getDropdownIEAlignmentTest()
    {
        return binder.bind(AuiDropdown2.class, By.id("dropdown2-test-ie-threshold-trigger"));
    }

    public AuiDropdown2 getSubmenuEditDropdown()
    {
        return binder.bind(AuiDropdown2.class, By.id("submenu-edit-trigger"));
    }
}
