# Atlassian User Interface (AUI) Library

AUI is an open source library. You can include it as a dependency in Atlassian add-on projects; You can also download it as a _flat pack_ archive. To prototype, you can use the online [Sandbox Tool](http://docs.atlassian.com/aui/latest/sandbox/) directly or from within a flat pack download.

__Note__: The flat pack does not contain the AUI components that rely on backend functionality.

# Atlassian Design Guidelines (ADG)

The [ADG](https://developer.atlassian.com/design/) is built with AUI. It explains and demonstrates the design components and principles behind the Atlassian UI. The Atlassian UX team developed the ADG. The team regularly updates and usability tests the guidlelines. Follow the ADG to leverage the work of experienced designers and make a beautiful Atlassian UI.

# Getting Started with AUI and ADG

For a tutorial that teaches how to use AUI as either a flat pack or as a plugin dependency, see [Getting Started with AUI](https://developer.atlassian.com/display/AUI/Getting+Started+with+AUI). 

If you don't feel the need for a tutorial, you can:

1. [Download the flat pack from Atlassian's maven repository](https://maven.atlassian.com/content/groups/public/com/atlassian/aui/aui-flat-pack/). 

2. Unzip the flat pack.

3. Launch the `aui.html` page in a browser to get started.


## Additional Documentation
* [Component documentation](http://developer.atlassian.com/display/AUI/)
* [Sandbox Tool](http://docs.atlassian.com/aui/latest/sandbox/)
* [Release Notes](https://developer.atlassian.com/display/AUI/AUI+Release+Notes)

# Discover which AUI Version your Atlassian Application is running

Most Atlassian products already provide AUI, ready for use. To check the AUI version:

1. Open the Atlassian application in your browser.

2. Launch a browser script console.

3. Type `AJS.version` at the prompt. 


# AUI Development or How to Contribute to AUI

AUI's own build uses the Atlassian reference application (REFAPP) for development and testing. There is a [guide to setting up an AUI development environment](https://developer.atlassian.com/display/AUI/Setting+up+a+dev+environment+for+AUI) in the docs.

See the [AUI Contributor Guide](https://developer.atlassian.com/display/AUI/AUI+Contributor+Guide) for more details about contributing code to AUI.

# License

[See the "licenses" directory for license information about AUI and included libraries](https://bitbucket.org/atlassian/aui-archive/src/master/licenses/).