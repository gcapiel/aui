Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/toolbar.js');

// Toolbar JS only loads for IE8 and below
var isIE = ( AJS.$.browser.msie && parseInt(AJS.$.browser.version, 10) < 9 ),
    hasFirstClass,
    hasLastClass;

module("Toolbar Unit Tests", {
    setup: function(){
        if(isIE) {
            AJS.setUpToolbars();
        }
    }
});

if (isIE) {
    
    test("Toolbar IE first classes", function() {
        hasFirstClass = AJS.$(".aui-toolbar .toolbar-group .toolbar-item:first-child").hasClass("first");
        equal( hasFirstClass, true, "First children should have class 'first'" );
    });       
    
    test("Toolbar IE last classes", function() {
        hasLastClass = AJS.$(".aui-toolbar .toolbar-group .toolbar-item:last-child").hasClass("last");
        equal( hasLastClass, true, "Last children should have class 'last'" );
    });       

} else {
    
    test("Toolbar", function() {
        ok(true, "Toolbar has no testable javascript for this browser version");
    });       

}
