Qunit.require('js/external/jquery/jquery.js');
Qunit.require('js/external/jquery/plugins/jquery.aop.js');
Qunit.require('js/external/raphael/raphael.js');
Qunit.require('js/atlassian/raphael/raphael.shadow.js');
Qunit.require('js/atlassian/atlassian.js');
Qunit.require('js/atlassian/dropdown.js');

(function () {

var dropdown;

module("Dropdown Unit Tests", {
    setup: function () {
        dropdown = AJS.$("#dropdown-test").dropDown("Standard")[0];
    },
    teardown: function () {
        dropdown.hide();
    }
});

test("Dropdown Creation", function() {
    var testDropdown = AJS.dropDown("test","standard");
    ok(typeof testDropdown == "object", "dropdown object was created successfully!");
});

test("Dropdown move down", function() {
    AJS.$("#dropdown-test .aui-dd-trigger").click();
    dropdown.moveDown();
    var dropdownItems = AJS.$(".dropdown-item"),
        selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    console.log(selectedItem[0]);
    console.log(dropdownItems[0]);
    equal(selectedItem[0], dropdownItems[0], "First item selected");

    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[1], "Second item selected");

    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[2], "Third item selected");
    
    dropdown.cleanActive();
    dropdown.moveDown();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[0], "First item selected again");
});

test("Dropdown move up", function() {
    AJS.$("#dropdown-test .aui-dd-trigger").click();
    dropdown.moveUp();
    var dropdownItems = AJS.$(".dropdown-item"),
        selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[2], "Third item selected");

    dropdown.cleanActive();
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[1], "Second item selected");

    dropdown.cleanActive(); 
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[0], "First item selected");
    
    dropdown.cleanActive(); 
    dropdown.moveUp();
    selectedItem = AJS.$(".dropdown-item.active");
    equal(selectedItem.length, 1, "Only one item selected");
    equal(selectedItem[0], dropdownItems[2], "Third item selected again");
});
})();
