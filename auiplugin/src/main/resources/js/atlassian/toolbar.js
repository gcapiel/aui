AJS.setUpToolbars = function () {
    if (AJS.$.browser.msie) {
        
        var toolbarGroups = AJS.$(".aui-toolbar .toolbar-group");
       
        // Fix left,right borders on first, last toolbar items
        toolbarGroups.each(function (i, group) {
            AJS.$(group).children(":first").addClass("first");
            AJS.$(group).children(":last").addClass("last");
        });

        // IE7 spoon feeding zone.
        if (parseInt(AJS.$.browser.version, 10) == 7) {

            // Add a class so we can style button containers
            function markItemsWithButtons () {
                AJS.$(".aui-toolbar button").closest(".toolbar-item").addClass("contains-button");
            };
  
            // force right toolbar to new row when it will fit without wrapping
            function forceRightSplitToRow() {
                AJS.$(".aui-toolbar .toolbar-split-right").each(function(i, right) {
    
                    var splitRight = AJS.$(right),
                        splitToolbar = splitRight.closest(".aui-toolbar"),
                        splitLeft = splitToolbar.find(".toolbar-split-left"),
                        leftWidth = splitToolbar.data("leftWidth"),
                        rightWidth = splitToolbar.data("rightWidth");
   
                    if(!leftWidth) {
                        leftWidth = splitLeft.outerWidth();
                        splitToolbar.data("leftWidth", leftWidth);
                    }
                    if (!rightWidth) {
                        rightWidth = 0;
                        AJS.$(".toolbar-item", right).each(function (i, item) {
                            rightWidth += AJS.$(item).outerWidth();
                        });
                        splitToolbar.data("rightWidth", rightWidth);
                    }
                    var toolbarWidth = splitToolbar.width(),
                        spaceAvailable = toolbarWidth - leftWidth;
    
                    if ( toolbarWidth > rightWidth && rightWidth > spaceAvailable ) {
                        splitLeft.addClass("force-split");
                    } else {
                        splitLeft.removeClass("force-split");
                    }
                });
            };

            // simulate white-space:nowrap because IE7 is refusing to do it right
            function simulateNowrapOnGroups () {
                toolbarGroups.each(function (i, group) {
                    var groupWidth = 0;
                    AJS.$(group).children(".toolbar-item").each(function (i, items) {
                        groupWidth += AJS.$(this).outerWidth();
                    });
                    AJS.$(this).width(groupWidth);
                });
            };

            // IE7 inits
            simulateNowrapOnGroups();
            markItemsWithButtons();
            
            // fire forceRightSplitToRow after reload
            var TO = false;
            AJS.$(window).resize(function(){
                if(TO !== false)
                    clearTimeout(TO);
                    TO = setTimeout(forceRightSplitToRow, 200);
            });
        }
    }
};

AJS.toInit(function() {
    AJS.setUpToolbars();    
});