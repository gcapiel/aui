package com.atlassian.pageobjects.aui.component.dropdown2;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.utils.by.ByJquery;
import com.google.common.base.Preconditions;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.List;
/**
 * @deprecated Previously published API deprecated as of AUI 5.0. Do not use outside AUI. Will be refactored out eventually.
 */
@Deprecated
public class AuiDropdown2Items
{
    @Inject
    PageElementFinder elementFinder;
    
    @Inject
    PageBinder binder;
    
    private final By dropdownLocator;
    
    private PageElement dropdown;

    public AuiDropdown2Items(By dropdownLocator)
    {
        this.dropdownLocator = dropdownLocator;
    }
    
    @Init
    public void init()
    {
        dropdown = elementFinder.find(dropdownLocator);
    }
    
    public AuiDropdown2Item get(int index)
    {
        List<PageElement> dropdownItems = dropdown.findAll(ByJquery.$("> ul > li, > .aui-dropdown2-section > ul > li"));

        Preconditions.checkState(index < dropdownItems.size(), "Dropdown item not found at index: " + index + ", list is of size: " + dropdownItems.size());

        return binder.bind(AuiDropdown2Item.class, dropdownItems.get(index));
    }
//
//    public AuiDropdown2Item get(By locator)
//    {
//
//    }

}
